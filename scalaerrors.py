import sublime, sublime_plugin, os, re, sys, time, threading, json, logging, queue
from random import randint

print(os.path.join(os.path.dirname(__file__), "watchdog"))
# sys.path.append(os.path.join(os.path.dirname(__file__), "watchdog"))
sys.path.insert(0, os.path.dirname(__file__))

from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from watchdog.events import PatternMatchingEventHandler
from watchdog.events import FileSystemEventHandler

class MyFileWatcher(FileSystemEventHandler):
    def __init__(self, watch_path, queue):
        self.watch_path = watch_path
        self.queue = queue
        self.regex = re.compile("\[(?P<type>.+)\] (?P<file>.+)(?=:\d+):(?P<line>\d*): (?P<message>.*)")

    def process(self, event):
        if not hasattr(self, 'my_number'):
            self.my_number = randint(0, 1000)
        print("my file watcher " + str(self.my_number), event.src_path, event.event_type)
        quickfix_file_path = self.watch_path + os.sep + "sbt.quickfix"
        if event.src_path.strip() == quickfix_file_path.strip(): 
            quickfix_file = open(quickfix_file_path, 'r')
            quickfix_contents = quickfix_file.read()
            quickfix_file.close()
            result = self.regex.findall(quickfix_contents)
            self.queue.put(("new_result", result))

    def on_modified(self, event):
        self.process(event)

class FileWatcher(threading.Thread): 
 
    def __init__(self, watch_path, signal_end_event, scala_queue):
        self.watch_path = watch_path
        self.signal_end_event = signal_end_event
        self.scala_queue = scala_queue
        threading.Thread.__init__(self)
 
    def run(self):
        print("running FileWatcher at: ", self.watch_path)
        event_handler = MyFileWatcher(self.watch_path, self.scala_queue)
        observer = Observer()
        observer.schedule(event_handler, self.watch_path, recursive=True)
        observer.start()
        print("going to start naow")
        while not self.signal_end_event.isSet():
            time.sleep(1)
        observer.stop()
        observer.join() 
 
class ScalacCenter(threading.Thread):
    def __init__(self, signal_end_event, queue):
        self.signal_end_event = signal_end_event
        self.queue = queue
        self.results = []
        threading.Thread.__init__(self)

    def run(self):
        while not self.signal_end_event.isSet():
            item = self.queue.get()
            print("ScalacCenter pulled from queue", item)
            if item[0] == "new_result":
                # put results somewhere
                self.results = item[1]

                # get all windows
                # for each window get active view
                # check if active view is file that has errors
                # if yes, display them
                for window in sublime.windows():
                    active_view = window.active_view()
                    print("the active_view", active_view.file_name())
                    for error in item[1]:
                        if error[1] == active_view.file_name():
                            print("display0", active_view.file_name())
                            self.display_errors(active_view)
            elif item[0] == "result_request":
                print("display1", item[1].file_name())
                self.display_errors(item[1])

    def display_errors(self, view):
        print("display_errors", view)
        newlines = [] # array of newline beginnings
        error_regions = [] #array of (type, region)

        for error in self.results:
            if not view.file_name().strip() == error[1].strip():
                print("continuing", view.file_name(), error[1])
                continue
            #1: get newlines for file if not found
            allcontent = sublime.Region(0, view.size())
            file_as_string = view.substr(allcontent)

            file_newlines = [0]
            last = -1
            while True:
                last = file_as_string.find('\n', last + 1)
                if last == -1:
                    break
                file_newlines.append(last + 1)
            file_newlines.append(len(file_as_string))
            newlines = file_newlines

            line = int(error[2])
            newline_at_line = newlines[line-1]
            newline_as_region = sublime.Region(newline_at_line, newline_at_line)

            error_regions.append((error[0], newline_as_region))
        print("error_regions:")
        print(error_regions)

        for_warnings = [region_tuple[1] for region_tuple in error_regions if region_tuple[0] == "warn"]
        print("for_warnings", for_warnings)
        view.add_regions("scalac_warning123", for_warnings, "string", "dot") # | sublime.PERSISTENT

        for_errors = [region_tuple[1] for region_tuple in error_regions if region_tuple[0] == "error"]
        print("for_errors", for_errors)
        view.add_regions("scalac_error", for_errors, "storage", "dot")

       #  file_as_view.set_status('sublimelinter', "asdf")

# todo also enable when new window created
# todo for some reason watchdog throws an exception when the plugin is reloaded, but 
# everything seems to work so ... who cares? :>
signal_end_events = [] 
scala_queue = queue.Queue()

class FileChangeListener(sublime_plugin.EventListener):
    def on_activated(self, view):
        if view.file_name() is not None:
            print("on_activated:", view.file_name())
            print("scala_queue:", FileChangeListener.scala_queue)
            FileChangeListener.scala_queue.put(("result_request", view))

def plugin_unloaded():
    print("unloaded", signal_end_events)
    for e in signal_end_events:
        e.set()  

def plugin_loaded(): 
    scala_queue = queue.Queue()
    FileChangeListener.scala_queue = scala_queue
    end_event = threading.Event()
    scalac_center = ScalacCenter(end_event, scala_queue)
    scalac_center.start()
    signal_end_events.append(end_event)

    for window in sublime.windows():
        proj_data = window.project_data()
        if "settings" in proj_data:
            if "scalac" in proj_data["settings"]:
                proj_path = os.path.join(proj_data["folders"][0]["path"], "target", "quickfix")
                print("signal_end_events", signal_end_events)
                end_event = threading.Event()
                file_watcher = FileWatcher(proj_path, end_event, scala_queue)
                file_watcher.start()
                signal_end_events.append(end_event)
