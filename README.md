1. clone into ~/path to sublime 3/Packages/
2. add the folder for the project that you're working on to your sublime window
3. create a new file (sbl_proj.sublime-project) in the root folder of the project with the following content (adjust filepath to the real project folder):

```
#!json

{
    "folders":
    [
        {
            "follow_symlinks": true,
            "path": "C:\\Developer\\Scala\\SbtCompileToFile\\QuickFixTesting"
        }
    ],
    "settings":
    {
        "scalac": true
    }
}


```